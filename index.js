// Bài 1 : Tính ngày tháng

function ngayhomqua() {
    var ngay = document.getElementById('ngay').value * 1
    var thang = document.getElementById('thang').value * 1
    var nam = document.getElementById('nam').value * 1
    var ngayqua = 0
    var thangqua = 0
    

    if (ngay == 1 && thang == 5 && thang == 7 && thang == 10 && thang == 12) {
        ngayqua = 30
        thangqua = thang - 1
    } else if (ngay == 1 && thang == 2 && thang == 4 && thang == 6 && thang == 8 && thang == 9 && thang == 11) {
        ngayqua = 31
        thangqua = thang - 1
    } else if (ngay == 1 && thang == 3) {
        if (nam % 400 == 0) {
            ngayqua = 29
            thangqua = 2
        } else {
            ngayqua = 28
            thangqua = 2
        }
    } else if (ngay == 1 && thang == 1) {
        ngayqua = 31
        thangqua = 12
        nam = nam - 1
    } else{
        ngayqua = ngay - 1
        thangqua = thang
    }

    document.getElementById('tinhngay').innerHTML = `${ngayqua}/${thangqua}/${nam}`
}



function ngaymai() {
    var ngay = document.getElementById('ngay').value * 1
    var thang = document.getElementById('thang').value * 1
    var nam = document.getElementById('nam').value * 1
    var ngaysau = 0
    var thangsau = 0

    if (ngay == 31 && thang == 1 && thang == 3 && thang == 5 && thang == 7 && thang == 8 && thang == 10) {
        ngaysau = 1
        thangsau = thang + 1
    } else if (ngay == 30 && thang == 4 && thang == 6 && thang == 9 && thang == 11) {
        ngaysau = 1
        thangsau = thang + 1
    } else if (ngay == 31 && thang == 12) {
        ngaysau = 1
        thangsau = 1
        namsau=nam+1
    }else {
        ngaysau=ngay+1
        thangsau=thang
        namsau=nam
    }

    console.log(ngaysau)
        nam ++
        document.getElementById('tinhsau').innerHTML=`${ngaysau}/${thangsau}/${namsau}`
}   


// Bài 2:
function tinhngay(){
    var thang = document.getElementById('text-thang').value * 1
    var nam = document.getElementById('text-nam').value * 1
    var songay=0
    if (thang==1||thang==3||thang==5||thang==7||thang==8||thang==10||thang==12){
        songay=31
    }else if (thang==4||thang==6||thang==9||thang==11){
        songay=30
    }else if(thang==2){
        if (nam%400==0){
            songay=29
        }else{songay=28}
    }else {
        songay ="không có giá trị hợp lý"
    }
   document.getElementById('tinhsongay').innerHTML=songay
}

// bài 3
 function docso() {
    var so=document.getElementById('so').value*1
    var hangtram=Math.floor(so / 100)
    var hangchuc=Math.floor(so / 10) % 10
    var donvi=so % 10
 if(hangtram==1){
    doctram= 'Một Trăm'
   }else if(hangtram==2){
    doctram= 'Hai Trăm'
   }else if(hangtram==3){
    doctram= 'Ba Trăm'
   }else if(hangtram==4){
    doctram= 'Bốn Trăm'
   }else if(hangtram==5){
    doctram= 'Năm Trăm'
   }else if(hangtram==6){
    doctram= 'Sáu Trăm'
   }else if(hangtram==7){
    doctram= 'bảy Trăm'
   }else if(hangtram==8){
    doctram= 'Tám Trăm'
   }else if(hangtram==9){
    doctram= 'Chín Trăm'
   }
   if(hangchuc==1){
    docchuc= 'Một Mươi'
   }else if(hangchuc==2){
    docchuc= 'Hai Mươi'
   }else if(hangchuc==3){
    docchuc= 'Ba Mươi'
   }else if(hangchuc==4){
    docchuc= 'Bốn Mươi'
   }else if(hangchuc==5){
    docchuc= 'Năm Mươi'
   }else if(hangchuc==6){
    docchuc= 'Sáu Mươi'
   }else if(hangchuc==7){
    docchuc= 'Bảy Mươi'
   }else if(hangchuc==8){
    docchuc= 'Tám Mươi'
   }else if(hangchuc==9){
    docchuc= 'Chín Mươi'
   }else if(hangchuc==0){
    docchuc= 'Không'
   }
 if(donvi==1){
    docdv= 'Một'
   }else if(donvi==2){
    docdv= 'Hai'
   }else if(donvi==3){
    docdv= 'Ba'
   }else if(donvi==4){
    docdv= 'Bốn'
   }else if(donvi==5){
    docdv= 'Năm'
   }else if(donvi==6){
    docdv= 'Sáu'
   }else if(donvi==7){
    docdv= 'bảy'
   }else if(donvi==8){
    docdv= 'Tám'
   }else if(donvi==9){
    docdv= 'Chín'
   }else if(donvi==0){
    docdv= ''
   }
   console.log(docdv)
   console.log(docchuc)
   console.log(doctram)
 document.getElementById('docso1').innerHTML=`${doctram} ${docchuc} ${docdv}`
}